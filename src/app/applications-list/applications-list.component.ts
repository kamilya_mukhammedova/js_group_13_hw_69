import { Component, OnDestroy, OnInit } from '@angular/core';
import { UserFormService } from '../shared/user-form.service';
import { Subscription } from 'rxjs';
import { UserForm } from '../shared/user-form.model';

@Component({
  selector: 'app-applications-list',
  templateUrl: './applications-list.component.html',
  styleUrls: ['./applications-list.component.css']
})
export class ApplicationsListComponent implements OnInit, OnDestroy {
  applicationsChangeSubscription!: Subscription;
  applicationsFetchingSubscription!: Subscription;
  isFetching = false;
  applications!: UserForm[];

  constructor(private userFormService: UserFormService) { }

  ngOnInit(): void {
    this.applicationsChangeSubscription = this.userFormService.applicationsChange.subscribe((applications: UserForm[]) => {
      this.applications = applications;
    });
    this.applicationsFetchingSubscription = this.userFormService.applicationsFetching.subscribe((isFetching: boolean) => {
      this.isFetching = isFetching;
    });
    this.userFormService.fetchApplications();
  }

  ngOnDestroy(): void {
    this.applicationsChangeSubscription.unsubscribe();
    this.applicationsFetchingSubscription.unsubscribe();
  }
}
