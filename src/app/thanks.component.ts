import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-thanks',
  template: `
    <h2 class="mt-5">Your registration is accepted, thank you!</h2>
    <button
      type="button"
      class="btn btn-success"
      (click)="onAllApplications()"
    >
      See all applications
    </button>
  `,
  styles: [`
  h2 { color: red }
  `]
})
export class ThanksComponent {
  constructor(private router: Router) {}

  onAllApplications() {
    void this.router.navigate(['/applications']);
  }
}
