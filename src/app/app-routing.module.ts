import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ThanksComponent } from './thanks.component';
import { UserFormComponent } from './user-form/user-form.component';
import { ApplicationsListComponent } from './applications-list/applications-list.component';
import { ApplicationDetailsComponent } from './application-details/application-details.component';
import { ApplicationResolverService } from './shared/application-resolver.service';

const routes: Routes = [
  {path: '', component: UserFormComponent},
  {
    path: 'applications', component: ApplicationsListComponent, children: [
      {
        path: ':id',
        component: ApplicationDetailsComponent,
        resolve: {
          application: ApplicationResolverService
        }
      },
      {
        path: ':id/edit',
        component: UserFormComponent,
        resolve: {
          application: ApplicationResolverService
        }
      },
    ]
  },
  {path: 'thanks', component: ThanksComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
