import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { UserFormComponent } from './user-form/user-form.component';
import { ValidatePhoneNumberDirective } from './user-form/validate-phoneNumber.directive';
import { HttpClientModule } from '@angular/common/http';
import { UserFormService } from './shared/user-form.service';
import { ThanksComponent } from './thanks.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { ApplicationsListComponent } from './applications-list/applications-list.component';
import { ApplicationDetailsComponent } from './application-details/application-details.component';

@NgModule({
  declarations: [
    AppComponent,
    UserFormComponent,
    ThanksComponent,
    ValidatePhoneNumberDirective,
    ToolbarComponent,
    ApplicationsListComponent,
    ApplicationDetailsComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [UserFormService],
  bootstrap: [AppComponent]
})
export class AppModule { }
