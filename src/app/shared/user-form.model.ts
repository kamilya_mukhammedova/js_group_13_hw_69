export class UserForm {
  constructor(
    public id: string,
    public name: string,
    public surname: string,
    public patronymic: string,
    public phoneNumber: string,
    public jobOrStudyPlace: string,
    public tShirtVariant: string,
    public size: string,
    public comment: string
  ) {}
}
