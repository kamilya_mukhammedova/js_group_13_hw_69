import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { UserForm } from './user-form.model';
import { map, tap } from 'rxjs/operators';

@Injectable()
export class UserFormService {
  userFormUploading = new Subject<boolean>();
  applicationsFetching = new Subject<boolean>();
  applicationsChange = new Subject<UserForm[]>();
  applicationRemoving = new Subject<boolean>();

  private applicationsArray: UserForm[] = [];

  constructor(private http: HttpClient) {}

  fetchApplications() {
    this.applicationsFetching.next(true);
    this.http.get< {[id: string]: UserForm }>('https://kamilya-61357-default-rtdb.firebaseio.com/form.json')
      .pipe(map(result => {
        if (result === null) {
          return [];
        }
        return Object.keys(result).map(id => {
          const applicationId = result[id];
          return new UserForm(
            id,
            applicationId.name,
            applicationId.surname,
            applicationId.patronymic,
            applicationId.phoneNumber,
            applicationId.jobOrStudyPlace,
            applicationId.tShirtVariant,
            applicationId.size,
            applicationId.comment
          );
        });
      }))
      .subscribe(applications => {
        this.applicationsArray = applications;
        this.applicationsChange.next(this.applicationsArray.slice());
        this.applicationsFetching.next(false);
      }, () => {
        this.applicationsFetching.next(false);
      });
  }

  fetchApplication(id: string) {
    return this.http.get<UserForm | null>(`https://kamilya-61357-default-rtdb.firebaseio.com/form/${id}.json`)
      .pipe(map(result => {
        if(!result) {
          return null;
        }
        return new UserForm(
          id,
          result.name,
          result.surname,
          result.patronymic,
          result.phoneNumber,
          result.jobOrStudyPlace,
          result.tShirtVariant,
          result.size,
          result.comment
        );
      }));
  }

  addUserForm(form: UserForm) {
    const body = {
      name: form.name,
      surname: form.surname,
      patronymic: form.patronymic,
      phoneNumber: form.phoneNumber,
      jobOrStudyPlace: form.jobOrStudyPlace,
      tShirtVariant: form.tShirtVariant,
      size: form.size,
      comment: form.comment,
    };
    this.userFormUploading.next(true);
    return this.http.post('https://kamilya-61357-default-rtdb.firebaseio.com/form.json', body)
      .pipe(tap(() => {
        this.userFormUploading.next(false);
      }, () => {
        this.userFormUploading.next(false);
      }));
  }

  editApplication(form: UserForm) {
    this.userFormUploading.next(true);
    const body = {
      name: form.name,
      surname: form.surname,
      patronymic: form.patronymic,
      phoneNumber: form.phoneNumber,
      jobOrStudyPlace: form.jobOrStudyPlace,
      tShirtVariant: form.tShirtVariant,
      size: form.size,
      comment: form.comment,
    };
    return this.http.put(`https://kamilya-61357-default-rtdb.firebaseio.com/form/${form.id}.json`, body)
      .pipe(tap(() => {
        this.userFormUploading.next(false);
      }, () => {
        this.userFormUploading.next(false);
      }));
  }

  removeApplication(id: string) {
    this.applicationRemoving.next(true);
    return this.http.delete(`https://kamilya-61357-default-rtdb.firebaseio.com/form/${id}.json`)
      .pipe(tap(() => {
        this.applicationRemoving.next(false);
      }, () => {
        this.applicationRemoving.next(false);
      }));
  }
}
