import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';
import { UserFormService } from '../shared/user-form.service';
import { UserForm } from '../shared/user-form.model';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit, OnDestroy {
  @ViewChild('f') userForm!: NgForm;
  userFormUploadingSubscription!: Subscription;
  isUploading = false;
  isEdit = false;
  editedId = '';
  commentMaxLength = 300;
  commentLength = 0;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private userFormService: UserFormService,
  ) { }

  ngOnInit(): void {
    this.userFormUploadingSubscription = this.userFormService.userFormUploading.subscribe((isUploading: boolean) => {
      this.isUploading = isUploading;
    });
    this.route.data.subscribe(data => {
      const application = <UserForm | null>data.application;
      if(application) {
        this.isEdit = true;
        this.editedId = application.id;
        this.commentLength = application.comment.length;
        this.setFormValue({
          name: application.name,
          surname: application.surname,
          patronymic: application.patronymic,
          phoneNumber: application.phoneNumber,
          jobOrStudyPlace: application.jobOrStudyPlace,
          tShirtVariant: application.tShirtVariant,
          size: application.size,
          comment: application.comment
        });
      } else {
        this.isEdit = false;
        this.editedId = '';
        this.setFormValue({
          name: '',
          surname: '',
          patronymic: '',
          phoneNumber: '',
          jobOrStudyPlace: '',
          tShirtVariant: '',
          size: '',
          comment: ''
        });
      }
    });
  }

  setFormValue(value: {[key: string]: any}) {
    setTimeout(() => {
      this.userForm.setValue(value);
    });
  }

  sendForm() {
    const id = this.editedId || Math.random().toString();
    const userForm = new UserForm(
      id,
      this.userForm.value.name,
      this.userForm.value.surname,
      this.userForm.value.patronymic,
      this.userForm.value.phoneNumber,
      this.userForm.value.jobOrStudyPlace,
      this.userForm.value.tShirtVariant,
      this.userForm.value.size,
      this.userForm.value.comment
    );
    if(this.isEdit) {
      this.userFormService.editApplication(userForm).subscribe(() => {
        this.userFormService.fetchApplications();
        void this.router.navigate(['..'], {relativeTo: this.route});
      });
    } else {
      this.userFormService.addUserForm(userForm).subscribe(() => {
        void this.router.navigate(['/thanks']);
      });
    }
  }

  onInputEvent() {
    this.commentLength = this.userForm.value.comment.length;
  }

  ngOnDestroy(): void {
    this.userFormUploadingSubscription.unsubscribe();
  }
}
