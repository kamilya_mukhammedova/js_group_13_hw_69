import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserFormService } from '../shared/user-form.service';
import { UserForm } from '../shared/user-form.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-application-details',
  templateUrl: './application-details.component.html',
  styleUrls: ['./application-details.component.css']
})
export class ApplicationDetailsComponent implements OnInit, OnDestroy {
  application!: UserForm;
  applicationRemovingSubscription!: Subscription;
  isRemoving = false;

  constructor(
    private route: ActivatedRoute,
    private userFormService: UserFormService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.route.data.subscribe(data => {
      this.application = <UserForm>data.application;
    });
    this.applicationRemovingSubscription = this.userFormService.applicationRemoving.subscribe((isRemoving: boolean) => {
      this.isRemoving = isRemoving;
    });
  }

  onRemove() {
    this.userFormService.removeApplication(this.application.id).subscribe(() => {
      this.userFormService.fetchApplications();
      void this.router.navigate(['..'], {relativeTo: this.route});
    });
  }

  ngOnDestroy(): void {
    this.applicationRemovingSubscription.unsubscribe();
  }
}
