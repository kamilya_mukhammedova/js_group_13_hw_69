import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { UserForm } from './user-form.model';
import { EMPTY, Observable, of } from 'rxjs';
import { UserFormService } from './user-form.service';
import { mergeMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApplicationResolverService implements Resolve<UserForm> {

  constructor(private userFormService: UserFormService, private router: Router) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<UserForm> | Observable<never> {
    const applicationId = <string>route.params['id'];
    return this.userFormService.fetchApplication(applicationId).pipe(mergeMap(application => {
      if(application) {
        return of(application);
      }
      void this.router.navigate(['/applications']);
      return EMPTY;
    }));
  }
}
